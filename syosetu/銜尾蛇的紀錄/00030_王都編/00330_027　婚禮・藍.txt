從他那裡被託付的工作，果然還是太嚴苛了。

首先作為臨時服侍貴族的人這種事情就是害羞的事情、要求學習作為宅邸的侍女的禮法。走路、說話的方式，面對對方的身份做出的相應禮法、都要充分的學習。向她教授禮儀的侍女、明顯的輕蔑自己。因為是奴隷。是不能夠稱之為人類的持有物、因此就像是要破壊她一樣的命令著她、千方百計給她那些難以完成的工作。只要犯一點錯誤就會被毫不留情的痛罵、被痛罵也成為了這個卑賤的身體每天的例行公事。

然而她連一言半句也沒有反抗。糾結教育的手法和內容都是沒有意義的事情、比什麼都重要的是主人的事情。為了他、只需要為了他就夠了。就像是接受被賦予的名字一樣，她吸收著各種各樣的內容。過了三年的歳月、她終於開始了服侍主人的工作了、她雖然年紀還小、但是卻能夠說出比其他侍女還要能幹這種能夠毫不慚愧的話來。

以此同時身體的鍛鍊也開始了。開始，只是一味地在野地裡奔跑。為了能夠承受得了今後的修練、他說基礎的身體素質是必要的。就像是煉鋼工作一樣。就像是打鐵趁熱這句諺語、為了不讓其折斷、不讓其出現裂痕、出現削弱的情況、必須先加固其存在。在那程度的體力練習之後、劍術的基礎練習也開始了。為了彌補在手腳不夠長的不足、開始了劍術的練習、徹底的基礎反復練習。之後又接受了他的命令、接受了作為她教練的男人的指導。

她一次都沒有表現出弱氣的一面。如果變的更強、如果變的更快、如果表現的更好、就能夠看都主人歡喜的表情了。那都是為了他、一切都是為了他。那個賦予她新名字的人、便是她能夠持續不懈進行修練的動力。過了一年的時間、那個主人請來的教練发的酒量開始越來越大了、她也有著不會輸給普通大人的力量了。

體術和劍技已經熟練了之後、又開始了魔法與野伏的教育。她適應性最高的魔法、是風與地這二種屬性。進行教授的、是一位作為冒険者的女性、對於她能夠適應相反的屬性非常的吃驚。野伏的訓練，某種意義上是其他的要嚴酷。對於修行來說就是一天裡不停的消除跡象潛伏著。這期間、其他的教育都不能接受。

但她一點也沒有疏忽。她對自己的術進行磨練、魔力不斷的提高、技法也精湛起來、野伏的程度、能夠保護主人的範圍也越來越大。那都是為了他、一切都是為了他。為了那個賦予她新名字的人、她如同消化一樣不斷的學習著。四年的時間過去了、當達到了主人的請來的老師的實力後、她為了滅口讓他們消失了。

最辛苦的、是擔任錬金術的助手。對於藥的製法、初歩的知識學習、製作作為資金來源的藥水。這些都是簡單的。但是、主人用那些錢購買了奴隷、進行了人體實驗。與自己一樣帶著項圈的人、沒有一個活著。那是把他們當做消費品的做法、可怕的行為。主人把他們全部殺了、自己也學習著他們殺了。自己殺了他們並沒有害怕的感覺。像他們一樣、對主人來說變成沒用的存在、才是害怕的原因。這個身體是為了他而復甦、這個心是因為他才從破碎的樣子修復的。比起這些東西、想要成為對他來說更加有意義的物品。再這樣的想法下、開始了更加專心致志的研鑽。

彼女即使胃裡的東西都到的喉噥時也沒有露出表情。她面對主沒有露出任何畏懼的表情、成為了其他奴隷所效仿的對象。那都是為了他、一切都是為了他。就像是被那個賦予的名字一樣、她的罪孽越積越重。過了四年的時間、在主人的實驗發揮了成果的時候、她一人敲響了冒険者公會的大門。

他，是她的主人，是慎重的男人。對她表現的忠誠口中雖然讚揚，但是內心是絶對無法相信的。對她露出寵愛的喜悅表情，對於她面對考驗的不懈努力的讚揚，某種程度來說也是一種考驗。其原因、諷刺的是因為她優秀過頭了。

對於完成了無數考驗，每次的力量都不斷增強的她，即使連一萬億的可能都沒有，但是還是能夠瞬間殺死他。

他害怕死，尋求著永遠。為此甚至可以犧牲世上所有人的、就是對不死的境界如此渇望。因此害怕遠遠超過了有用這個限度的她。所以讓她離開、讓遠遠超過他的計算的她離開、讓不知何時會背叛自己成為殺死自己的刀刃離開。事實上在那種情況下、就算是想要處分她也絶對不會疑惑吧。

⋯⋯討厭。

死了還比較好。為了他而犧牲的末路、本來就期盼這種事情。但是、對於成為阻礙他的目的這種事情十分討厭。

因為她、只是是為了幫助他而出生的。
某日，她請求了他。

「主人、拜託你了」
「什麼事？」
「請讓我、成為完全的奴隷」

叛逆等多餘的功能。
那些根源的多餘因子。
把那些全部除掉、向主人請求。

⋯⋯他很高興地笑著。

「⋯⋯啊哈」

這是發自心底的笑容。他是一個沒有微笑的溫厚的少年，那全部都是面具。真正的笑容，只有這一次。
從取下纏著的綳帶以來，她出生的那一天。
從那天以來，看到眼前浮現的笑容。她情不自禁地按住了胸。

「那句話──從得到你的那一天開始，就一直在期待著」

啊啊，她又吐了氣。
理解、並同意了。
一切都是為了讓自己說這句話而存在的。
溫熱的飯菜，也有甜美的點心，乾淨的床舖，洗淨身體的奢侈，稱讚的言詞，寬大的寬恕⋯⋯一切的一切，為了這句話。
然後她又後悔了自己的發言。

「⋯⋯讓您久等了，再次給您添麻煩了」

要早一點說就好了。

※※※

「呀、哥哥。一年不見了！」

返回從前懷念的家裡的我，盡量像看起來心情愉快地發出了聲音。
哥哥特地在大廳的玄關裡迎接。因為是我、所以並不會有失禮儀。其他有著一些零零散散的暢談的賓客的身影。恐怕、是為了迎接遠方的客人的意思。

「托利烏斯、來了嗎。遠路遙遠、明日の祝儀が為に參ってくれたこと、辛苦了」

那樣說的、是跟一個輕浮的貴公子一樣笑的自熱的哥哥、萊納斯。
這是看起來充滿了令人高興的兄弟的重逢，不過，哥哥說「辛苦了」，沒有一句「高興」。呀哩呀哩、一年過去了也不像大人的人。真的能夠過好婚後生活嗎？

「這一次成婚，實在是恭喜你們了。我也很高興地對你說。哥哥是本家的頂梁柱。要是身體平安，作為弟弟的我也輕鬆。」
「哈哈哈⋯⋯開玩笑、托利烏斯。我還是年輕人。這個說法、簡直就像我的婚期遲到了一樣」
「這真是失禮了。對我來說、這個身體還要多多保養」

僕的即興會話結束的同時、後面的杜耶和魯貝爾似乎在說著什麼。

「⋯⋯翻譯，拜託了」
「嗯，首先閣下說『これから結婚生活なんだから、もう余計なことはしないでくれ』と仰って、兄君は『斷る。それに私の結婚がこの年にまで伸びたのは誰の所為だと思っている？』と返されましたね」

真是厲害、魯貝爾。

總之，兄長今後也不打算改變態度。嘛、只要那個侯爵陪著、這種希望就不大。
然後，與其他客人的暢談的婦女打破沉默進來入了。

「哎呀，太過分了。對弟弟的寒暄，沒有什麼我能說的」
「希莫娜──」
「《小姐》是多餘的，你」

從口氣來看，著這個人是哥哥的妻子，未來會是我義姐的女性。或者說、上司。要先表現禮貌嗎。

「初次見面。我是萊納斯・歐布尼爾伯爵的弟弟、托利烏斯・疏日南・歐布尼爾子爵」
「哎呀，真是客氣」

女性稍微意外地瞪大眼睛。大概，是聽說過我的壊傳聞吧。殺奴隷、不詳細了解貴族禮儀什麼的。嘛、確實是事實。

「那麼這邊也⋯⋯來自蓬托邦男爵家、我的名字是希莫娜・梅莉亞。明天、將會成為你哥哥的妻子的人。今後也請多多關照、子爵殿下？」

彼女措辭看來是洗練了無數次、確實有著貴婦人的威嚴。為名的姓氏沒有聽過，但父母應該是熱心教育的人。另外還是一個五官端正的美人。哥哥也抓到了好老婆。對於被我搞臭家裡的名聲、真是可惜了這個人。
考慮那樣的事，接受自報後進行了深深的鞠躬。

「這邊也請多多關照、伯爵夫人」
「啊、哎依、有什麼好寒暄的嗎？有什麼事情嗎？」
「僕の目には我らが領主閣下⋯⋯に見えますね。もしかして、知らない內に目玉も改造されていたのかな⋯⋯」

⋯⋯杜耶、魯貝爾。聽到了嗎？

特別是魯貝爾。我都已經那麼老實了、千萬不要出什麼亂子。
千萬不要說出什麼失禮的話來、對於這邊的問候義姐可是用溫柔的微笑回應了。

「沒有什麼謠言，意外的可靠嗎？出色的義弟真是讓人高興」
「沒有沒有。我在這個王都居住時還小、做了各種年少輕狂的事情。玷污的你的耳朵、真是抱歉」

言畢、回想起王都時代。以前真是沒有效率的實驗方法。以現在的本事、可是有著能夠增加百分之三十的消費量的本事呢。

「咳」

突然，哥哥的咳嗽聲打破氣氛。

「比起今天，明天姐弟打打關係也不錯、托利烏斯、你也是帶著家臣的。讓他們自我介紹」

就像是要很快地打完招呼後去房間裡一樣。是想別讓新娘從愚弟里接受奇怪的事情嗎？。

「是的，讓他們等著太過分了⋯⋯魯貝爾、杜耶。向哥哥打招呼」
「是」
「應、じゃなくて⋯⋯ははっ」

被招呼的二人走到哥哥前面，單膝跪地。

「能夠在伯爵閣下面前說出自己的名字、惶恐至極。我是約翰・傑克・魯貝爾。作為閣下的弟弟的部下、做著官吏的工作」
「は、伯爵大人出來拜見、真是難得。杜耶・舒華澤、那個、在你弟麾下的武官！」

與毫無結舌的魯貝爾對比、杜耶的話有點可怕。與普通人比起來。下次回去時要好好教一教他。
哥哥看著杜耶的樣子揉著太陽穴，力求無視的樣子看了魯貝爾一眼。

「魯貝爾、你說話的語氣。難道說、魯貝爾是出生男爵家嗎？」
「是的。是家裡的四男」
「聽說如果記憶正確，那是一個有學識的年輕人。今後也請你支撐我弟弟」
「這溢美之詞，讓我無比榮幸」

表面上畢恭畢敬的魯貝爾、但是不知道內心裡在想什麼。如果你知道的話，你就雇傭了好了，一定是這樣的想法⋯

「那麼、杜耶・舒華澤嗎？好像有和你見過面的記憶」
「好好！過去那段日子無禮了！」

事前に維克托爾や魯貝爾から噛んで含めておかれたセリフを、カチコチになりながら言う。
一年前啟行的時候、哥哥耍流氓一樣的做了那種事情。僕もあの時は、まさか杜耶がまた兄と會う日が來るとは思ってなかったので、好きに言わせておいたのだった。嘛、他的最終調整結束後、也沒有指導他說話的時間。

「可以了。如果反省著，那就要一一挑剔的完成」
「哦，謝謝」

不習慣的說話方式、抽動著嘴角的杜耶。

「話說真是意外呢？閣下加入了弟弟的臣下的行列」
「お、畏れながら⋯⋯誒哆⋯⋯や、野卑な身を過分にも、お、御引立ていただだ、頂いておりまして、ですね⋯⋯」

喂喂、怎麼跟改造前的夏爾一樣？
哥哥意外的話。他這種方法很難對付、一旁的人大概是看明白的吧。なのに、小さくクスリという聲を漏らしつつ、まだ續けようとしている。
想著究竟有誰能夠阻止事態的發生。

「啊啦？」

希莫娜小姐她、好像是注意到了我們的情況、拖了個尾音。
在那裡、周圍跪著的有一個完全平伏的女僕的身影。

嘛、不用說就是優妮。

貴族的我是低頭、下級貴族與平民的杜耶、魯貝爾是下跪、奴隷的她則必須平伏。雖然很殘酷、但是這是國家的法律、作為規則在這個世界運行著。

「她是？」
「不好意思、她是我的奴隷女僕」
「嘛！」

對我的回答、希莫娜小姐眼睛瞪得圓圓的。

優妮是奴隷。屬於身份差別中社會的最底層。那樣的存在，做出禮法和慣例之類的舉動，是這個世界上的常識。在婚禮現場之類的地方，基本上醒目是不允許的。能否帶入、更是討論之外。

但是，我卻不知道那樣的事。其他的奴隷現在外待機著，有著婚禮的時候不讓進入的規定，不過，只讓優妮一人進入的話可以破例。作為我最信任的護衛兼服侍，哥哥和嫂嫂說什麼都要放在身邊。

嘛、如果真的是那麼討厭的話也可以讓優妮向忍者一樣潛入天花板就是了。

「對不起，請允許她在這個宅院裡逗留」
「在進行婚禮的地上，奴隷？」

果然希莫娜小姐也不能隱藏困惑。看來作為貴族是接受了正經教育的人，這也是理所當然的反應。倒不如，這才是正常的類型。

「服侍的、不是她不可以嗎？」
「是的、她是在我身邊最長的人」
「作為參考能不能讓我聽聽、有多少時間了？」
「這個嘛、十一年了」
「這麼多嗎？」

聽到我口中的年數、發出了一聲輕輕的驚訝聲。就是那樣子吧。我八歳的時侯購買的。優妮那時候六歳。這是相當長的時間啊、這是一個非常棒的奴隷。

「就是因為這樣子，在我的身邊最了解的人是她。再問一次，是否允許？」
「那樣⋯⋯我不介意。那、你呢？」

希莫娜看向哥哥那裡。

「什麼啊⋯⋯不對、什麼？」
「你也不是要給我寬恕的機會嗎？那樣的事，也沒關係嗎？」

哦哦、真是能幹的女性。哥哥真的是找到了一個好老婆。不對、從嫁給哥哥這點來看、應該是被逼迫嫁給哥哥的。
如果可以的話，希望哥哥的陰謀在這時候給我停下來，但是，只有她一個人是不行的。從父母家的門第來看、想要在當家事解決是不可能的、就算是背地裏協商解決也是不可能的。

現在也是在計算著、我哥哥的臉就是這樣表現的。

「⋯⋯隨你喜歡」

他那樣說著，轉了過去。

？

奇怪。因為哥哥的性格，就算接受，我想也會更加討厭的。
總之、哥哥可是一直討厭著在我旁邊的優妮的。難道像維克托爾說的一樣、與那位拉瓦萊侯爵相處久後改變了、發生了這種事情。不過這是不可能的、看著長年與我進行試驗討厭的優妮的他。怎麼可能怎麼容易就接受？

不過也沒有上當的感覺，新娘顯示出雅量這樣想可以嗎？。

「謝謝。對二人深切的慈悲、再次宣誓努力忠孝」
「⋯⋯呼」

對我致禮的哥哥的回答，是不快地吹鼻子。

⋯⋯為什麼這麼討厭我啊。從以前我不喜歡哥哥了、如果謀殺不行的話那就進行洗腦吧。畢竟風險高是第一理由。不過哥哥啊、我確實是搞臭了家裡的名聲、但是就因為如此就要殺我什麼的。實在是意味不明。

第一、我已經前往瑪爾蘭了、想辦法把那個名義上的伯爵領地再生了。在稅收上也有好好地按哥哥和國家規定的收取。雖然隱藏了重要礦山一事。嘛、不過這也算回報了、多少用來彌補王都時代的痛楚吧。我敵人減少了也會輕鬆很多。

總之在我自己心中，為兄弟的爭執而心疼、這時希莫娜小姐不合禮儀地走到了優妮旁邊。

「請你抬起臉」
「是。但是、以卑賤的身軀──」
「我命令你抬起臉」

在這樣的要挾下、優妮抬起了頭。
這不是義姉的高傲、也不是優妮的自卑。因為奴隷和貴族的互動，一般就是這樣的。

「⋯⋯っ」

看到優妮的臉的希莫娜小姐，不由得屏住了呼吸。

就是這樣吧，就算主人的我不偏袒、優妮也是十分的漂亮。從最初買的時候看到的那個殘酷的樣子，也無法想像會那麼漂亮、連最初討厭我的人看到優妮的樣子、也會被她的美貌──啊、純粹就是討人喜歡的臉、除此之外──沒有其他意思。

義姉、先是困惑的沉默了一下、然後開口。

「⋯⋯妳的、名字是？」
「優妮。家名、沒有」
「你服侍了他十一年、作為女僕？」
「是的。另外、也有些助理」

聽到優妮的回答、困惑的希莫娜按住太陽穴。然後、從容的轉到我的方向。

「托利烏斯卿」
「是的、有什麼事伯爵夫人」
「她，打算一直做奴隷嗎？」
「⋯⋯哈？」

等一下，話不對頭。
面對眨著眼睛的我，她盛大地嘆了口氣。

「那個、嗯。托利烏斯卿。比起明天以義姉的身份進行對話、現在先預先忠告」
「哈」
「你，究竟是怎樣的打算？把服侍十年以上的女性、當做卑賤之物」

希莫娜夫人、剛才的溫柔使用就像是說謊的一樣現在則是尖著聲。
生氣了？為什麼？

「雖然還未婚、有著身分差是沒辦法的事情。但是、怎麼可以給女性用奴隷這種犬貓一樣的身份？至少要有個比較適當的方法──」
「⋯⋯嗯、魯貝爾。這個婦人在說什麼？」
「嗯⋯⋯大概、估計是對領主閣下與女僕長之間的關係、有什麼誤會吧？」

在被說教的同時在後面用意義不明的目光看著我的、是不知道在說什麼的杜耶。
啊、你也怎麼認為嗎。
在他的話中、也是有著我對她的待遇的不滿吧、大概就是這意思。

「那個、伯爵夫人。不、義姉。好像稍微有些不對⋯⋯」
「不對？」

話語的攻擊終於停止了。
太好了、即使善意的女性的話、也會讓人不舒服呢。
安心後、繼續解釋。

「我和優妮、那個、什麼呢⋯⋯失禮了、並不是你想的那種關係」

所以說、這並不是那種誤解的所謂男女關係。所以、實話實說。

「⋯⋯哈？」

就像是鳩吃了豆鉄砲一樣的表情的希莫娜小姐。接著、表現出了露骨的驚訝表情。

「真的？」
「真的哦」
「那樣⋯⋯明明是個很漂亮的孩子？」
「這個我承認，但是沒有到那種關係。」

確實是可愛的奴隷。多少是有點感情的、但是沒有走到那一步的記憶。大體上、做了那做事情導致肚子過重的話、會導致她的能力大幅度下降。雖然避免這個事情發生的方法也是有的、但是無法百分之百保證。要是發生了就不是單單墮胎就能完事的、會對她身體造成相當的負擔、對於消去感情的她會有什麼負面影響還是個未知數。

那個女孩的身體、在我的慾望滿足為止、都無法找到其他東西代替良。嘛、如果我自己不老不死的願望實現的話、那麼就是另外一回事了。

閑話莫題。

希莫娜小姐像是看著動物園的珍獸一樣的目光看著我。她也是貴族的女人。也有學習嫁後的義務、嘴巴含著什麼東西等等方法的教授吧。那個教育就是、房中術──更加直白的說、就是滿足男人的性──比如活塞運動什麼的。要是以這個來考慮的話、確實我的話實在是難以置信。

看那樣子、哥哥也是瞠目結舌。或者說、臉變成藍色了。有那麼意外嗎？在學院時代的某個女性、對於我與優妮的關係可是非常了解的。這種「你這個怪人。作為人類而言、缺了什麼不可思議的東西」一樣的眼神。實在是非常失禮啊。

「⋯⋯信じられない」

因為為難的原因轉過了頭、這又是麻煩。
必須對初次見面的，明天就要嫁給哥哥的女性說些什麼。

「不過即使這樣說，也沒有什麼證明」
「也是⋯⋯那麼、這麼做」

好像至今難以理解，不過還是想辦法退出了話題了。相安無事、從某種程度上來說算是得救了。雖說我缺乏作為貴族的修養、但是在這個零零散散的玄關口、就不要說那種話了。

「我還以為你已經做了。因這樣的事情而被挽留，失禮了」
「沒有沒有。在這婚儀的晚上、有點傷神也是沒辦法的」

交換道歉後，我就離開了。
雖然有些冒失的地方，不過是個非常好的女性。一定要和哥哥建立幸福的家庭。就算因此變得多少有些圓滑、讓我輕鬆一些也好。
但是從哥哥說的話，聽到這個話題之後就變得有些奇怪。究竟是做了什麼讓弟弟不知道的事情、拘泥在那裡嗎。

※※※

「對不起啦。沒有謹慎的發言」
「那種事情不在乎。說起來、因為在那種狀況下還能夠保持平穩的狀態的人本來就很少」

在希莫娜安慰的同時、萊納斯心中狂躁不安。
那個該死奴隷殺手、違背人倫的惡魔、居然說沒有和那個女奴隷做過？怎麼可能？
或者說因為是人類當然的慾望、所以那傢伙反而沒有也說不定、即使如此⋯⋯⋯

禁不住想起，拉瓦萊侯爵第一次進入邸內的那一日。

那一天、被侯爵所幹倒的萊納斯、把憤怒發泄到了女奴隷身上。多麼的不合適、把暴力的衝動用那種方式解消、只能說是瘋了。反省後、那就事是人生中屈指可數的污點。雖然自己不想那樣子、然而之後也有好幾次、把無法抑制的怒火發泄到奴隷身上。

那個女奴隷、現在也在屋敷裡。
無法遠離侯爵、放出密探又無效。話說回來、找不到適當的理由殺了弟弟、有種墮落成為弟弟的同類這種討厭的感覺。

弟弟的、同類。
說不定已經到了那個地步了吧。以這樣想、萊納斯就覺得有無法想像的恐怖。

（⋯⋯不一樣）

試著搖頭、把這疑念給驅除掉。
貴族與奴隷交往、這種程度的事情不用皺眉頭就可以決定、不允許。反過來說托利烏斯把奴隷殺死也是可以的。也沒有觸碰到法律、頂多只會讓弱勢群體見者傷心，聞者落淚而已。

所以、自己、萊納斯、比弟弟要好──

『雖然還未婚、有著身分差是沒辦法的事情。但是、怎麼可以給女性用奴隷這種犬貓一樣的身份？至少要有個比較適當的方法──』

希莫娜的言詞、把矛頭指向弟弟。
萊納斯覺得、那才是應該與奴隷保持的關係。
一開始就沒有尋常的禮法、跟犬貓一樣已經是比較溫和的做法了。
作為奴隷的女人、有為自己的身體考慮的權利嗎？想想都不可能。
如果、希莫娜要是知道那個的話？

「嗚、庫⋯⋯！？」

禁不住捂住嘴。
覺得惡心想吐。

「沒問題吧？臉色好像不太好⋯⋯」

擔心的希莫娜的聲音。
她是不可能喜歡萊納斯的。突然的婚約，突然的結婚，根本培養不出甜美的愛情。不如說、利用婚禮來陷害弟弟這件事、還被鄙視了。
但是，她是一個公平的女性。面對剛才那個，噁心的女奴隷，也起了惻隱之心。對弟弟的固執而到處奔走的他，萊納斯的那個作為貴族以外的作為也表示理解。
但是如果、對女奴隷做出的行為被知道了、那麼那份溫柔瞬間也會消失吧。

「什麼都、沒有⋯⋯」

為了自己，只能這樣回答。
是的、什麼也沒有。

原來和睦相愛的婚姻，一開始就不認為會有。用兩家的血統組成夫婦留下下一代，只不過是男女之間的一種協定。雙方之間出現愛的萌芽、只能說是僥倖。首先結婚的結果，是從那裡開始尋找名為婚姻的妥協點。
ならば、元から嫌われている身である。そこに新たな汚點が見つかろうと、氣に掛ける方がどうかしている。

希莫娜悟出這邊頑固的話、發出小小的嘆息。

「⋯⋯這樣的話請表現好一點。婚儀前青著臉、不就好像我欺負你一樣？」
「抱歉⋯⋯」
「即便那樣──」

然後、希莫娜眼光變得銳利了起來。

「弟弟的存在還是不能允許嗎？」

對弟弟的偏執表現，表現出了明確厭惡的表情的希莫娜。

即使不愛著對方，貴族的女人也是看重婚禮場合的。教養的高貴品格、對於半生青澀的果實來說。那是一生一度的舞台。有的人也有過幾次經驗，不過那也是例外。在這樣重要的活動中，卻染上了陰謀的顏色，只要是正經的女性，都會感到不快。

但是萊納斯也有了覺悟去執行陰謀了。對於男人來說萬一婚儀的神聖性面前讓陰謀暴露的話、這個立場是不行的。

「當然。天地神明面前、那個惡魔是不允許存在的⋯⋯！」

正是有了那個決心，才有這樣利用婚禮開始到陰謀。

「所以說、他的事就那麼可憎嗎？」
「事到如今還說什麼。你也知道吧、他究竟這個屋子究做了什麼事情、你應該有聽過吧」

無論如何在希莫娜娘家的蓬托邦家、作為女兒的她之所以出嫁就是因為家道中落已經成為了寒門。那個對象的貴族家裡的事情是知道的。當然、因為那個愚弟所留下的惡名、已經充分理解了。事實上、訂婚的當日、她甚至有種硬著頭皮踏入魔王城的覺悟。

「確實，他的壊傳聞都聽過了。但是這也是，以前的事情了吧？」
「以前的、事情？」

對那個言詞、萊納斯的目光變得銳利起來。

「那時、一年前下來讓他趕往瑪爾蘭的命令的時候、他可是就跟臨別時留下的麻煩土產一樣把奴隷全部殺了？臨別時匆匆忙忙解除的！就用一年的時間、他能有多大改變！？」

現在想起來都害怕。被宣告出發的時候、托利烏斯下了殺死奴隷於將研究的地下室關閉的命令、把那裡剩餘的奴隷全部殺掉、並且細致的火葬。這背後究竟在進行了什麼實驗、才會有如此大量的奴隷。

如果考慮那個、那個叫杜耶的粗魯男人、居然能夠很正常的服侍托利烏斯。他還聽說起行之前在那個屋子裡住了一夜。那副光景也應該是看到了。究竟是什麼神經、才會去服侍那種狂人啊。

面對粗暴張聲的萊納斯、她送出了像是責備一樣的視線。

「噓！小聲點」

突然那個變成了警句。這裡是大廳的玄關。說不定有著幾組閑談的賓客剛好路過的可能、。事實上、有些正準備給新郎祝賀的、正用懷疑的眼光看著自己。

「⋯⋯抱歉」
「道歉待會再說。的確有可能一年前開始會遮遮掩掩的做著、但是領主工作也好好完成了吧？事到如今想要讓所有人都排斥他可能性、覺得有多少」
「⋯⋯」
「還有、揭發不品行的行為、用違背王國法律的明確做法進行制裁、再說高等法院的工作吧。無論如何、你我實施陰謀來對付他的原因、也是沒道理的──」
「就到此為止」

萊納斯、舉起手打斷了那個發言。
確實她的話是正確的。但是、如果正確的做法就能忍耐的話、從這邊向弟弟的殺意就沒有了。
其理由是、在家裡萊納斯的受到了太大的打擊。托利烏斯惡奴隷殺手汚名所帶來的危害相比、那個鄉下建立的在怎麼好、也無法彌補。

「歐布尼爾的家長是我。屬於家長的我、沒有必要聽你的意見」

因此屬於家長的萊納斯，決定排除托利烏斯。
那個決定、不是一個月前剛剛訂婚的小姑娘能夠顛覆的。

「是嗎⋯⋯」

放棄改變主意了嗎，希莫娜退了半步之後對話就停止了。
萊納斯生氣的吹著鼻子、重新勒緊領帶。

她想什麼，做什麼，明天的婚禮策劃的侯爵的陰謀是不能阻止的了。只知道的是，擬定者本人是萊納斯而已。什麼都不知道的她、也無法教托利烏斯任何方法、就算把疑念告訴高等法院、也無法起到任何作用。這個策略最狡猾的一點、就是完全沒有違法性。被看穿也只能因為失敗而沮喪而已、托利烏斯絶對沒有逆襲的方法。

希莫娜想什麼、做什麼都跟萊納斯沒關係。她對大局一點影響都沒有。只能夠迎接舞台的組成、記錄這件事情而已。極端的說、拉瓦萊侯爵陣營已經完事了。
當然、萊納斯的嗣子也會作為棋子被培育、成為維持歐布尼爾家陣營的重要道具。

道具。
是啊、道具。丈夫希望嗣子如同他想的發展、成為這個家族的道具。對貴族的男人來說妻子並不是必要的、只是為了實現目的的手段罷了。

無意間，想起了母親的臉。
不被父親關注的活著、把那個惡魔產下後便丟下性命的女性。
那個悲哀的臉，與希莫娜的聲影重疊。

（我，和父親做著一樣的事情⋯⋯）

因無能而愚鈍，冷待自己追求優秀孩子的父親。讓母親操勞而死、讓那個怪物肆意抹黑家庭的愚者。
在這如出一轍的認知上，胃部再次疼痛，喉嚨裡的東西硬升上來。

「嗚、庫⋯⋯」

把手放到嘴邊，忍耐著噁心。
連自己也明白了臉已經變得藍了。

（不一樣⋯⋯！才不一樣⋯⋯！）

自己應該與父親是不一樣。與那個學疏才淺的、冷待平庸的萊納斯、讓母親懷孕而死、產下托利烏斯膽怯而死去的愚者是不一樣的。
不如說、自己是要把歐布尼爾的內患給消除掉──

──才被拉瓦萊侯爵利用、導致現在的情況才對？

（就是不一樣吧⋯⋯！？）

你與弟弟是一樣的、誰又能比過誰。

──弟弟殺死奴隷，你侵犯奴隷。有什麼不同？

你與父親是一樣的、誰又能笑話誰。

──你和父親都讓托利烏斯家被抹黑了。有什麼不同？

你的結局、也是成為歐布尼爾的男人、誰又能蔑視誰。

──歐布尼爾家的男人全部一樣。是貴族社會的恥辱、沒有任何存在價值。

嘲笑自己的是誰。蔑視自己的是誰。
希莫娜嗎？
母親嗎？
父親嗎？
拉瓦萊侯爵嗎？
亦或是托利烏斯？
亦或是⋯⋯自己？

（住口⋯⋯我是不一樣的！）

噁心越來越強大、萊納斯好幾次搖頭忍耐住。
幻覺。看見幻覺了。一定是托利烏斯所為。那傢伙總是進行奇怪的實驗，現在讓自己看到幻覺的藥物也開發出來了。如果托利烏斯不存在、自己就不用那麼痛苦。如果托利烏斯不存在、自己也會幸福的活著吧。

好幾次這樣對自己勸說，把噁心咽下。

⋯⋯希莫娜啊、這次無論說什麼我也不允許停止計劃。