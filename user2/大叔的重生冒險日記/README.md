# novel

- title: おっさんのリメイク冒険日記 ～オートキャンプから始まる異世界満喫ライフ～
- title_ja_old: おっさんがオートキャンプしてたら、何故か異世界でキャンプする羽目になった
- title_zh: 大叔的重生冒險日記～從露營開始盡情體驗異世界生活～
- author: 緋色優希
- illust: 市丸 きすけ
- source: http://ncode.syosetu.com/n6339do/
- cover: https://images-na.ssl-images-amazon.com/images/I/71UWRzDViXL.jpg
- publisher: syosetu
- date: 2019-07-20T18:00:00+08:00
- status: 連載
- novel_status: 0x0300

## authors

- トワイライト

## illusts

- 小池えいらく

## publishers

- syosetu

## series

- name: おっさんのリメイク冒険日記 ～オートキャンプから始まる異世界満喫ライフ～（旧題　おっさんがオートキャンプしてたら、何故か異世界でキャンプする羽目になった）

## preface


```
コミカライズがcomicブースト様で連載されています。https://comic-boost.com/series/95
ツギクルブックス様から書籍第６巻が８月１０日に発売されます。コミックス２巻が発売中。　
ある日、オートキャンプ中に次元の隙間から異世界へ行く羽目になったおっさんが、数々の冒険の果てにケモミミ幼稚なるものを創設することになった。色々な事件に巻き込まれつつも、様々なイベントを開催していくのであった。精霊・ドラゴン・エルフ・ドワーフ・ゴブリンと、色んな種族とも交流していく中での異色の異世界ライフ。若返ったおっさんが、やりたい放題のチートストーリー。そして世界は『おっさん』の物なのだった。６２００万ＰＶまで到達しました。いつも応援大変ありがとうございます。
```

## tags

- node-novel
- R15
- syosetu
- おチビ猫
- エルフ
- ケモミミ幼稚園
- コピー能力
- ダンジョン
- ハイファンタジー
- ハイファンタジー〔ファンタジー〕
- ベスマギル
- 冒険
- 恋愛
- 日本の食べ物
- 残酷な描写あり
- 王子様王女様
- 異世界転移
- 精霊
- 魔法
- 魔法ＰＣ
- ＳＳＳランク冒険者

# contribute


# options

## downloadOptions

- noFilePadend: true
- filePrefixMode: 1
- startIndex: 1

## syosetu

- txtdownload_id:
- series_id:
- novel_id: n6339do

## textlayout

- allow_lf2: true

# link

- [narou.nar.jp](https://narou.nar.jp/search.php?text=n6339do&novel=all&genre=all&new_genre=all&length=0&down=0&up=100) - 小説家になろう　更新情報検索
- [小説情報](https://ncode.syosetu.com/novelview/infotop/ncode/n6339do/)
- https://www.dm5.com/manhua-dashudechongshengmaoxianriji-congluyingkaishijinqingtiyanyishijieshenghuo/
- 


