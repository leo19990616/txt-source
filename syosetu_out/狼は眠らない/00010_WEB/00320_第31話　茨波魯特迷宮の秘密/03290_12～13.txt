１２

隔天早上。
天氣很好，雖然有些冷，但還是把桌子搬出來，在庭院旁吃早飯。

「真好吃呢，這個麵包」
「很好吃呢。是剛出爐的吧」
「庭院也很漂亮呢」
「是的。是個美麗的庭院」
「那個梅切露木，明明樹枝在庭院側伸得那麼長，為什麼迴廊側沒有伸阿？」
「是阻根喔」
「阻根？」
「在地面連續埋入大石頭，在那裡把根部擋住」
「喔？」
「梅切露木的粗枝和粗根，會往相同的方向伸出相同的長度。您看，伸到庭院側的粗枝的正下方伸出了粗根對吧」
「啊啊，確實是這樣」
「是利用這特性，在不想讓樹枝生長的地方阻根」
「呼嗯。但是種在那種角落不會擠得難受嗎」
「雖然控制樹枝的生長方式能展現造園士的手藝，但這有點在賣弄小聰明呢。比起這個，左側的白絆木很讓人在意」
「那個伸出了纖弱的白色細枝的小樹嗎？」
「是的」
「映照出了後面的綠色，真漂亮阿。形狀也很有趣」
「顏色的對比無話可說呢。不過白絆的成長很快。再過五年就會有現在的三倍或四倍的大小吧」
「嗯─。那麼大小的平衡不就會不好嗎」
「是的。所以，大概會在變大前拔起吧。然後種植新的樹。這庭院的整體就是這樣的結構喔」
「呼嗯。原來如此。話說回來，昨天睡得好嗎？」
「床很舒適呢。好久沒用那麼舒服的床單了」

把飯後茶放在手掌上，搖動著手，茶的高級香氣便輕輕撫過鼻孔。用了很高級的茶葉吧。
又看了庭院一次。

美麗的庭院。
宛如精打細算，從雷肯兩人現在的位置看的時候，能看到最好的對比。
但是，也並非感覺不到不自然。

「豆粒草不喜歡陰暗處就是了。種在那種位置的話，從這邊看不到的位置的葉子應該枯掉了吧」

豆粒草是藥草。在希拉的藥草田裡也有，諾瑪的診療所的庭院也有。因為是看慣的植物，所以很清楚這庭院的豆粒草被修剪得多仔細。

「造了這庭院的造園士，不知道白絆成長得很快嗎」
「怎麼可能。擁有這等技術的造園士，不可能不知道白絆的成長率」
「知道但還硬是去弄嗎」
「這雖然只是我的直覺」
「嗯？」
「是不是有人指示說，這裡要用這種形狀，種植這種植物之類的呢」

原來如此，如此想到。然後，想通了。
這庭院的構造、區劃的構造、建築的構造和迴廊的構造等，都非常協調。應該說，是以一個設計理念建造出整體的。說是以一個人格為主也行。

（我跟這傢伙合不來吧）

雷肯回想起了，沃卡城的諾瑪的施療所的庭院的草木。

枝葉生長得非常自在。配置雖然有點雜亂，但那大辣辣的種植方式，以及盡情生長的枝葉，能讓心情放鬆。
然後，想到了〈拉芬的岩棚亭〉的蔬菜田。那些蔬菜，是多麼得生氣勃勃。

「昨天的燻酒怎麼樣呢？」
「很美味。第一杯」
「只有第一杯嗎？」
「那種東西，從第二杯開始就都一樣吧」
「哈哈」
「昨天的肉很美味呢」
「是的。很美味呢。第一口」
「是阿」

兩人喝光了茶，放下看起來很貴的杯子。
寂靜的時候流過了一會。

「回去吧」
「回去吧」


１３

「經理，昨天的周到安排，非常感謝」
「哎呀，阿利歐斯大人。還有雷肯大人。就算不特地來接待處，這邊也會過去的。這是昨晚交付的物品的錢和鑑定書」

阿利歐斯瞄了瞄鑑定書後交給雷肯。

「謝謝。關於探索百階層以下，因為有說要住在〈錦嶺館〉一次，所以在此住宿了，真是間很棒的旅館。不論是調度、料理還是招待，都沒什麼好抱怨的」
「不敢當」

看了鑑定書的雷肯，注意到了某件事。

（嗯？）
（沒有寫〈深度〉）
（這是怎麼一回事？）

「話說回來，沒有規定說不能在這裡以外的地方住宿吧？」
「那是當然的。本旅館，僅僅是為了利用者的方便而已。不論要怎麼利用，還是不利用，都是利用者的自由」
「那就好。在探索的必要上，說不定有必要在別的宿屋商量。在這場合，應該不會有不回這裡的聯絡，沒關係嗎？」
「了解了」
「如果中意那邊的話，說不定會在那邊逗留一陣子」
「想回本旅館的話，隨時都歡迎歸還。話說回來，為方便聯絡，能告訴我要去哪裡就太感激了」
「還沒有完全決定下來，會在下次回到這裡時聯絡」
「遵命。那麼，請慢走」

是有一定會馬上回到〈錦嶺館〉的自信嗎，經裡沒有繼續探詢雷肯和阿利歐斯要去哪裡。
之後，兩人去了百階層，只戰鬥了一次就結束探索，並回到〈拉芬的岩棚亭〉。

「歡迎光⋯喔喔？阿利歐斯？還有雷肯？怎麼了。有什麼忘了拿嗎？」
「我回來了。上次付的長期住宿的住宿費，是到什麼時候呢」
「喔？等等。我看看帳簿。嗯嗯。到二月九日」
「那麼，總之先延長四十天」
「喔喔？」
「啊啦，小阿利歐斯。回來了嗎」
「是。又要再麻煩關照了」
「妮露」
「啊啊，雷肯先生。什麼事？」
「今天的晚飯，麻煩給我滿滿的美味蔬菜」
「好的好的。交給我吧」
「阿利歐斯。從明天開始，休養七天」
「請再說一次」
「我說休息七天。放鬆身心的緊張吧。之後就再開攻略」